BSGObj      = {}
BSGContour  = {}

tweak_data.contour.bsg = {
  low_ammo_color  = Vector3(1.0, 1.0, 0.45),
  damaged_color   = Vector3(0.61, 0.24, 0),
  danger_color    = Vector3(1.0, 0, 0)
}

function BSGContour.remove_hooks()
  if not Network:is_server() then
    Hooks:RemovePostHook('bsgcontour_damage_received_host_id')
  else
    Hooks:RemovePostHook('bsgcontour_damage_received_client_id')
  end
end

-- Add contours to ContourExt
function BSGContour.link_contours()
  Hooks:PostHook(ContourExt, "init", "bsgcontour_color_contour_init_id",
    function(...)
      ContourExt._types.bsg_low_ammo = {
        color = tweak_data.contour.bsg.low_ammo_color
      }
      ContourExt._types.bsg_damaged = {
        color = tweak_data.contour.bsg.damaged_color
      }
      ContourExt._types.bsg_danger = {
        color = tweak_data.contour.bsg.danger_color
      }
    end
  )
end

-- -- Sentry is deployed, check if you deployed it and add it to Sentry DB
function BSGContour.add_sentry()
  Hooks:PostHook(SentryGunBase, "post_setup", "bsgcontour_deploy_after_id",
    function (self, ...)
      local sentry_owner, peerID

      if self._unit.interaction then
        sentry_owner = self._unit:interaction()._owner_id
      end

      if managers.network.session then
        peerID = managers.network:session()._local_peer._id or 1
      end

      local my_sentry = sentry_owner == peerID

      if my_sentry then
        local uid   = self._sentry_uid
        BSGObj[uid] = {
          is_damaged  = false,
          is_ammo_low = false
        }
      end
    end
  )
end

function BSGContour.sentry_fire()
  Hooks:PostHook(SentryGunWeapon, "fire", "bsgcontour_fire_id",
    function (self, ...)
      local uid = self._unit:base()._sentry_uid

      if BSGObj[uid] and not BSGObj[uid].is_ammo_low then
        local ammo_total, ammo_max

        if Network:is_server() then
          ammo_total  = self._ammo_total
          ammo_max    = self._ammo_max
        else
          ammo_total  = self._virtual_ammo
          ammo_max    = self._virtual_max_ammo
        end

        if ammo_total and ammo_max then
          local ammo_pct      = math.ceil((ammo_total / ammo_max) * 100)

          local threshold     = BSGMod._settings.ammo_threshold_value
          local is_threshold  = ammo_pct <= threshold and ammo_pct > (threshold - 1)

          if is_threshold then
            BSGObj[uid].is_ammo_low = true

            if not BSGObj[uid].is_damaged then
              self._unit:sentry_contour():_set_contour('bsg_low_ammo')
            else
              self._unit:sentry_contour():_set_contour('bsg_danger')
            end
          end
        end
      end
    end
  )
end

-- Damage Taken func
function BSGObj.damage_contour(sg_unit, uid, hp_ratio)
  if BSGObj[uid] and not BSGObj[uid].is_damaged then
    local index         = BSGMod._settings.hp_threshold_value
    local hp_threshold  = BSGMod._hp_lookup[index]

    if hp_ratio <= hp_threshold then
      BSGObj[uid].is_damaged = true

      if not BSGObj[uid].is_ammo_low then
        sg_unit:sentry_contour():_set_contour('bsg_damaged')
      else
        sg_unit:sentry_contour():_set_contour('bsg_danger')
      end
    end
  end
end

-- Damage Taken - Host
function BSGContour.sentry_damaged_host()
  Hooks:PostHook(SentryGunEquipment, "_on_damage_received_event", "bsgcontour_damage_received_host_id",
    function (self, health_ratio)
      local uid = self._unit:base()._sentry_uid

      BSGObj.damage_contour(self._unit,uid,health_ratio)
    end
  )
end

-- Damage Taken - Client
function BSGContour.sentry_damaged_client()
  Hooks:PostHook(SentryGunDamage, "sync_health", "bsgcontour_damage_received_client_id",
    function(self, health_ratio)
      if self._unit and self._unit.base then
        local uid       = self._unit:base()._sentry_uid
        local hp_ratio  = health_ratio / 5

        BSGObj.damage_contour(self._unit,uid,hp_ratio)
      end
    end
  )
end

-- Remove sentry from DB
function BSGObj.remove(sg_unit)
  local uid = sg_unit._unit:base()._sentry_uid

  if BSGObj[uid] then
    BSGObj[uid] = nil
  end
end

-- Remove sentry from DB on Sentry Death
function BSGContour.sentry_died()
  Hooks:PostHook(SentryGunEquipment, "_on_death_event", "bsgcontour_death_id",
    function(self)
      BSGObj.remove(self)
    end
  )
end

-- Remove sentry from DB on Sentry Pickup
function BSGContour.sentry_pickedup()
  Hooks:PostHook(SentryGunEquipment, "_on_destroy_unit", "bsgcontour_destroy_id",
    function(self)
      BSGObj.remove(self)
    end
  )
end

if RequiredScript == 'lib/units/equipment/sentry_gun/sentrygunbase' then
  BSGContour.add_sentry()
elseif RequiredScript == 'lib/units/equipment/sentry_gun/sentrygundamage' then
  BSGContour.sentry_damaged_client()
elseif RequiredScript == 'lib/units/equipment/sentry_gun/sentrygunequipment' then
  BSGContour.sentry_damaged_host()
  BSGContour.sentry_died()
  BSGContour.sentry_pickedup()
elseif RequiredScript == 'lib/units/contourext' then
  BSGContour.link_contours()
else
  BSGContour.sentry_fire()
end
BSGContour.remove_hooks()