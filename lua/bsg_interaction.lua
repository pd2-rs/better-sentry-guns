BSGInteraction = BSGInteraction or {}

-- Main function
function BSGInteraction:init()
  local fm_dist     = BSGMod._settings.fm_dist_value * 100
  local fm_max_dist = tweak_data.interaction.sentry_gun.interact_distance

  tweak_data.interaction.sentry_gun_fire_mode.interact_distance     = fm_dist
  tweak_data.interaction.sentry_gun_fire_mode.max_interact_distance = fm_max_dist
end

-- Init when first sentry is deployed
Hooks:PostHook(SentryGunBase, "post_setup", "bsginteraction_post_setup_id",
  function(...)
    BSGInteraction:init()
    Hooks:RemovePostHook('bsginteraction_post_setup_id')
  end
)