local BSGFixes = {}

function BSGFixes.remove_hooks()
  if not Network:is_server() then
    Hooks:RemovePostHook('bsgfixes_hostage_id')
  end
end

-- OLD WAY: is_sentry > is_hostage > is_not_targeting_turret
-- NEw WAY: is_sentry > is_not_targeting_turret > is_hostage
--#region EXPERIMENTAL and HOST ONLY - Sentry Hostage Fix
function BSGFixes.hostage_fix()
  Hooks:PostHook(SentryGunBrain, '_attention_objective', 'bsgfixes_hostage_id',
    function(self, attention)
      -- attention.identified is needed to make sure it's being called from
      -- correct section of code
      if BSGMod._settings.hostage_fix_value and attention.identified then
        local is_sentry = BSGFixes.is_sentry(self)

        if is_sentry then
          local is_not_targeting_turret = BSGFixes.is_not_targeting_turret(attention)

          if is_not_targeting_turret then
            local is_hostage_not_nil = BSGFixes.is_hostage_not_nil(attention)

            if is_hostage_not_nil then
              local is_hostage = attention.unit:brain():is_hostage()
              if is_hostage then
                return "surrender"
              end
            end
          end
        end
      end
    end
  )
end
--#endregion

--#region Jack of all Trades Enhancement
function BSGFixes.pickup_fix()
  Hooks:PostHook(SentryGunEquipment, "_on_destroy_unit", "bsgfixes_pickup_id",
    function(self)
      if BSGMod._settings.pu_fix_value then
        local has_joat = managers.player:has_category_upgrade("player", "second_deployable")

        if has_joat then
          local selected = managers.player:selected_equipment()

          if selected then
            local eq            = selected['equipment']
            local is_not_sentry = eq ~= 'sentry_gun' and eq ~= 'sentry_gun_silent'

            if is_not_sentry then
              managers.player:switch_equipment()
            end
          end
        end
      end
    end
  )
end
--#endregion

--#region Dead Sentry Noise Fix
function BSGFixes.noise_fix()
  Hooks:PostHook(SentryGunEquipment, "_on_death_event", "bsgfixes_noise_id",
    function(self)
      if BSGMod._settings.noise_fix_value then
        if self._unit:character_damage()._turret_destroyed_snd then
          self._unit:character_damage()._turret_destroyed_snd:stop()
        end
        if self._broken_loop_snd_event then
          self._broken_loop_snd_event:stop()
        end
      end
    end
  )
end
--#endregion

--#region Mute Sentry Fire
function BSGFixes.mute_fire_fix()
  Hooks:PostHook(SentryGunWeapon, "_sound_autofire_start", "bsgfixes_mute_fire_id",
    function (self)
      if BSGMod._settings.mute_fire_fix_value then
        local is_sentry = BSGFixes.is_sentry(self)

        if is_sentry then
          self:_sound_autofire_end()
        end
      end
    end
  )
end
--#endregion

--#region Utilities
function BSGFixes.is_sentry(self)
  local type = self._unit:base()._type

  return type == "sentry_gun" or type == "sentry_gun_silent"
end

function BSGFixes.is_not_targeting_turret(attention)
  if attention.unit and attention.unit.base then
    return not attention.unit:base()._type
  end

  return false
end

function BSGFixes.is_hostage_not_nil(attention)
  if attention and attention.unit then
    if attention.unit.brain and attention.unit:brain() then
      return attention.unit:brain().is_hostage
    end
  end

  return false
end
--#endregion

if RequiredScript == 'lib/units/equipment/sentry_gun/sentrygunbrain' then
  BSGFixes.hostage_fix()
elseif RequiredScript == 'lib/units/equipment/sentry_gun/sentrygunequipment' then
  BSGFixes.pickup_fix()
  BSGFixes.noise_fix()
elseif RequiredScript == "lib/units/weapons/sentrygunweapon" then
  BSGFixes.mute_fire_fix()
end

BSGFixes:remove_hooks()