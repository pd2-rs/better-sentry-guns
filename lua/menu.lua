BSGMod              = BSGMod or {}
BSGMod._save_path   = SavePath .. 'better_sentry_guns.txt'
BSGMod._menu_path   = ModPath .. 'menu.txt'
BSGMod._loc_path    = ModPath .. 'loc/'
BSGMod._settings    = {
  fm_dist_value         = 8,
  ammo_threshold_value  = 35,
  hp_threshold_value    = 3,
  pu_fix_value          = true,
  noise_fix_value       = true,
  hostage_fix_value     = true,
  mute_fire_fix_value   = false
}
BSGMod._hp_lookup   = {0.2,0.4,0.6,0.8}

function BSGMod:load()
  local file = io.open(BSGMod._save_path, 'r')

  if file then
    local settings  = json.decode(file:read())

    for k, v in pairs(settings) do
      BSGMod._settings[k] = v
    end

    file:close()
  end
end

function BSGMod:save()
  local file = io.open(BSGMod._save_path, 'w')

  if file then
    local settings = json.encode(BSGMod._settings)

    file:write(settings)
    file:close()
  end
end

-- Load Settings
BSGMod:load()

-- Localization
function BSGMod:get_loc_file()
  if BLT and BLT.Localization then
    local lang = BLT.Localization:get_language().language
    local file = BSGMod._loc_path .. lang .. '.txt'

    if io.file_is_readable(file) then
      return file
    end
  end

  return BSGMod._loc_path .. 'en.txt'
end

Hooks:AddHook('LocalizationManagerPostInit', 'bsgmenu_loc_id',
  function(self)
    local loc_file = BSGMod:get_loc_file()
    self:load_localization_file(loc_file, false)
  end
)

-- Load menu
Hooks:Add("MenuManagerInitialize", 'bsgmenu_mmi_id',
  function(menu_manager)
    MenuHelper:LoadFromJsonFile(BSGMod._menu_path, BSGMod, BSGMod._settings)
  end
)

-- Callback Handlers - Interaction
MenuCallbackHandler.fm_dist_clbk = function(self, item)
  BSGMod._settings.fm_dist_value  = item:value()

  if BSGInteraction then
    BSGInteraction:init()
  end

  BSGMod:save()
end

-- Callback Handlers - Status Indicators
MenuCallbackHandler.ammo_threshold_clbk = function(self, item)
  local value = tonumber(item:value())

  BSGMod._settings.ammo_threshold_value = value
  BSGMod:save()
end

MenuCallbackHandler.hp_threshold_clbk = function(self, item)
  local value = tonumber(item:value())

  BSGMod._settings.hp_threshold_value = value
  BSGMod:save()
end

-- Callback Handlers - Fixes
MenuCallbackHandler.pu_fix_toggle_clbk = function(self, item)
  local value = item:value() == 'on'

  BSGMod._settings.pu_fix_value = value
  BSGMod:save()
end

MenuCallbackHandler.noise_fix_toggle_clbk = function(self, item)
  local value = item:value() == 'on'

  BSGMod._settings.noise_fix_value = value
  BSGMod:save()
end

MenuCallbackHandler.hostage_fix_toggle_clbk = function(self, item)
  local value = item:value() == 'on'

  BSGMod._settings.hostage_fix_value = value
  BSGMod:save()
end

MenuCallbackHandler.mute_fire_fix_toggle_clbk = function(self, item)
  local value = item:value() == 'on'

  BSGMod._settings.mute_fire_fix_value = value
  BSGMod:save()
end